﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;


namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioMascota" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioMascota.svc o ServicioMascota.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioMascota : IServicioMascota
    {

        
        public List<mascota> getMascotas()
        {
            try
            {
                List<mascota> Listmascota = new List<mascota>();
                var model = new ModelVeterinario();
                Listmascota = model.mascotas.ToList();
                return Listmascota;
            }
            catch (Exception) { }
            return new List<mascota>();
        }

        
        public mascota getMascota(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.mascotas.SingleOrDefault(p => p.id == id);
            }
            catch (Exception) { }
            return new mascota();
        }

        
        public Boolean AddMascota(mascota newMascota)
        {
            try
            {
                var model = new ModelVeterinario();
                model.mascotas.Add(newMascota);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        
        public Boolean ModifyMascota(mascota newMascota)
        {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newMascota).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }

    }
}
