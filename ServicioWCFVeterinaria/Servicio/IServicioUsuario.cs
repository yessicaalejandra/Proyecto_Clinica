﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicioWCFVeterinaria.Model;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioUsuario" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioUsuario
    {
        [OperationContract]
        [WebGet(UriTemplate = "Usuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<usuario> getUsuarios();

        [OperationContract]
        [WebGet(UriTemplate = "Usuario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        usuario getUsuario(int id);

        [OperationContract]
        [WebInvoke(UriTemplate = "Usuario", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddUsuario(usuario newUsuario);


        [OperationContract]
        [WebInvoke(UriTemplate = "Usuario", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyUsuario(usuario newUsuario);            
    }
}
