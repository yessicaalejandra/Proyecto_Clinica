﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioMascotaPropietario" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioMascotaPropietario.svc o ServicioMascotaPropietario.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioMascotaPropietario : IServicioMascotaPropietario
    {

        public List<mascotapropietario> getMascotaPropietarios() {
            try
            {
                List<mascotapropietario> ListmascotaPropietario = new List<mascotapropietario>();
                var model = new ModelVeterinario();
                ListmascotaPropietario = model.mascotapropietarios.ToList();
                return ListmascotaPropietario;
            }
            catch (Exception) { }
            return new List<mascotapropietario>();

        }


        public mascotapropietario getMascotaPropietario(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.mascotapropietarios.SingleOrDefault(p => p.fkmascota == id);
            }
            catch (Exception) { }
            return new mascotapropietario();
        }


        public Boolean AddMascotaPropietario(mascotapropietario newMascotaPropietario) {
            try
            {
                var model = new ModelVeterinario();
                model.mascotapropietarios.Add(newMascotaPropietario);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        public Boolean ModifyMascotaPropietario(mascotapropietario newMascotaPropietario) {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newMascotaPropietario).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }
    }
}
