﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioPropietario" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioPropietario.svc o ServicioPropietario.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioPropietario : IServicioPropietario
    {
        public List<propietario> getPropietarios()
        {
            try
            {
                List<propietario> Listpropietario = new List<propietario>();
                var model = new ModelVeterinario();
                Listpropietario = model.propietarios.ToList();
                return Listpropietario;
            }
            catch (Exception) { }
            return new List<propietario>();
        }


        public propietario getPropietario(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.propietarios.SingleOrDefault(p => p.id == id);
            }
            catch (Exception) { }
            return new propietario();
        }


        public Boolean AddPropietario(propietario newPropietario)
        {
            try
            {
                var model = new ModelVeterinario();
                model.propietarios.Add(newPropietario);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }


        public Boolean ModifyPropietario(propietario newPropietario)
        {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newPropietario).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }
    }
}
